# README #

Instead of using the complicated C version, consider using this python version  

Copied from [here](https://gist.github.com/vinothpandian/4337527)  
### Changes: ###
 * Changed all / to //  
 * Renamed window  
 * Messed with various global variables  
 * Made the left paddle computer-controlled  


### Steps to get this running: ###
 * Download the source code
 * [Download Python3](https://www.python.org/downloads/)
 * make sure to click the box that says to add Python to Path
 * from command line, type  
 	`python -m pip install pygame`  
 * navigate shell to pong.py location
 * run  
	`python pong.py`
